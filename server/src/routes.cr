require "kemal"
require "kemal-session"
require "./conf.cr"
require "./ssr.cr"
require "./gql.cr"

class HTTP::Server::Context
		def my_def
			p "Asdasdasd"
		end
end

# |middleware
# at any request cookie is maintained
# in ws case cookie is set at handshake phase [TODO - vulnerable?]
before_all "/*" do |env|
  begin
    Kemal::Session.new env
  rescue
    puts "No redis service."
  end
  if user_id = env.session.int?("user_id")
  end
end

get "/*" { |ctx|
  ctx.my_def
  jwt = "asd.asdas.d"
  ctx.response.headers["Authorization"] = "Bearer #{jwt}"
  ctx.request.path == GQL_ENDPOINT ? GQL.resp(ctx) : SSR.resp(ctx)
}

# Kemal::WebSocketHandler::INSTANCE.add_route path, &block
ws "/" { |sock, env|
  if key = env.response.cookies[COOKIE_NAME]?.try &.value
    p "connected ws #{key}"
  else
    p "malicious conn - aborting"
  end
}

