#
# session conf
#
COOKIE_NAME = "app"
# `crystal eval 'require "random/secure"; puts Random::Secure.hex(64)'`
COOKIE_SECRET = "e67e528f27e537ac2fb97e9643bf0c9689066a4c09cdd358a3d91a58afb9d756694ef1df64ed46a236ff2197db92bd368439f80cf61e5f794d8532e4df962579"
REDIS_SESSION_HOST = "redis_session"
REDIS_SESSION_PORT = 6379
REDIS_SESSION_PREFIX = ""

#
# routes conf
#
GQL_ENDPOINT = "/gql"

