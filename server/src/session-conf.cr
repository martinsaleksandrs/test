require "kemal-session"
require "kemal-session-redis"
require "./conf.cr"

# How long is the session valid after last user interaction?
Kemal::Session.config.timeout = Time::Span.new(6, 0, 0) # (6 hours)

# Name of the cookie that holds the session_id on the client
Kemal::Session.config.cookie_name = COOKIE_NAME

# How are the sessions saved on the server? (see section below)
Kemal::Session.config.engine = Kemal::Session::RedisEngine.new(
    host: REDIS_SESSION_HOST,
    port: REDIS_SESSION_PORT,
    key_prefix: REDIS_SESSION_PREFIX,
)

# In which interval should the garbage collector find and delete expired sessions from the server?
Kemal::Session.config.gc_interval = Time::Span.new(0, 1, 0) # (1 minutes)

# Used to sign the session ids before theyre saved in the cookie
Kemal::Session.config.secret = COOKIE_SECRET

# The cookie used for session management should only be transmitted over encrypted connections.
Kemal::Session.config.secure = false

# Domain to use to scope cookie
Kemal::Session.config.domain = nil

# Scope cookie to a particular path
Kemal::Session.config.path = "/"

