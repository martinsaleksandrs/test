FROM crystallang/crystal:0.26.0
WORKDIR /app

# install shards
COPY shard.yml ./shard.yml
# RUN shards install

# server
COPY src ./src

# install sentry
RUN apt-get update
RUN apt-get install curl
RUN curl -fsSLo- \
  https://raw.githubusercontent.com/samueleaton/sentry/master/install.cr | \
  crystal eval

# compile sentry
RUN crystal build --release dev/sentry_cli.cr -o ./sentry

# run sentry
CMD ./sentry --install

