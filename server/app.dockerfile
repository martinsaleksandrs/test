FROM crystallang/crystal:0.26.0
WORKDIR /app

# install shards
COPY shard.yml ./shard.yml
RUN shards install

# server
COPY src ./src

# compile app
RUN crystal build --release src/app.cr

# run app
CMD env CRYSTAL_ENV=production ./app

